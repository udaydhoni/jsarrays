function each(array,callBack) {
    for (let index =0 ; index<array.length ; index++) {
        callBack(array[index],index);
    }
}
module.exports = each;

