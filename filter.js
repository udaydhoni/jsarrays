function filter(array, callBack) {
    let newArray = [];
    for (let index = 0; index < array.length ; index++) {
        if (callBack(array[index])) {
            newArray.push(array[index]);
        }
    }
    return newArray;
}

module.exports =filter;