function find(array,callBack) {
    for (let index =0 ; index < array.length ; index++) {
        if (callBack(array[index])) {
            return array[index];
        }
    }
    return undefined;
}

module.exports = find;