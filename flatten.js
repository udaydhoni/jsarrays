function flatten (array) {
    let newArray = [];
    for (let index = 0 ; index<array.length ; index++) {
        if (typeof array[index] != 'object') {
            newArray.push(array[index]);
        } else {
            newArray.push(...flatten (array[index]));
        }
    }
    return newArray;
}

module.exports = flatten;