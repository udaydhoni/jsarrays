function map(array,callBack) {
    let newArray = [];
    for (let index = 0 ; index<array.length ; index++) {
        newArray.push(callBack(array[index]));
    }
    return newArray;
}

module.exports = map;