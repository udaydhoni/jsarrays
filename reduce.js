function reduce(array,callBack,startingValue) {
    if (startingValue === undefined) {
        startingValue = array[0];
        for (let index = 1 ; index < array.length ; index++){
            startingValue = callBack(startingValue,array[index]);
        }
    }else {
        for (let index =0; index<array.length ; index++) {
            startingValue = callBack(startingValue,array[index]);
        }
    }
    return startingValue;
}

module.exports = reduce;